#!/usr/bin/env python
# -*- coding: utf-8 -*-
    

import pandas as pd # provide sql-like data manipulation tools. very handy.
pd.options.mode.chained_assignment = None
import numpy as np # high dimensional vector computing library.
from copy import deepcopy
from string import punctuation
from random import shuffle
import pandas as pd       
import gensim
from sklearn.cross_validation import train_test_split
import numpy as np

import gensim
from gensim.models.word2vec import Word2Vec # the word2vec model gensim class
LabeledSentence = gensim.models.doc2vec.LabeledSentence 
from gensim.models import KeyedVectors


from tqdm import tqdm
tqdm.pandas(desc="progress-bar")  # to read the element of the array

from nltk.tokenize import TweetTokenizer 
tokenizer = TweetTokenizer()

from sklearn.model_selection import train_test_split
from sklearn.feature_extraction.text import TfidfVectorizer

from keras.callbacks import EarlyStopping
from keras.models import Sequential
from keras.layers.core import Dense, Dropout, Flatten
from keras.layers import LSTM, Bidirectional, Convolution1D
from keras.layers.embeddings import Embedding
from keras.preprocessing import sequence
from keras.preprocessing.text import Tokenizer
from keras.preprocessing.text import one_hot

from keras.layers.convolutional import Conv1D
from keras.optimizers import Adam
from keras.optimizers import SGD
from keras.callbacks import ModelCheckpoint


from bs4 import BeautifulSoup 
import re
import nltk
from nltk.corpus import stopwords # Import the stop word list
from nltk.corpus import sentiwordnet as swn
from nltk.classify import *
from nltk.stem import *
from nltk.stem.porter import *

import sys
import os
import time
import csv
import re

reload(sys)  
sys.setdefaultencoding('utf8')


import logging
logging.basicConfig(level=logging.DEBUG)

#text preprocessing
def clean_text(corpus):
    punctuation = """.,?!:;(){}[]"""
    corpus = [z.lower().replace('\n','') for z in corpus]
    corpus = [z.replace('<br />', ' ') for z in corpus]

    #treat punctuation as individual words
    for c in punctuation:
        corpus = [z.replace(c, ' %s '%c) for z in corpus]
    corpus = [z.split() for z in corpus]

 
    stemmer = PorterStemmer()

    for z in corpus:
        stemmed_words = [stemmer.stem(word) for word in z]
        z = " ".join(stemmed_words)


    return corpus

def clean_text_hashtag(corpus):
    punctuation = """.,?!:;(){}[]#@"""
    corpus = [z.lower().replace('\n','') for z in corpus]
    corpus = [z.replace('<br />', ' ') for z in corpus]

    #treat punctuation as individual words
    for c in punctuation:
        corpus = [z.replace(c, ' %s '%c) for z in corpus]
    corpus = [z.split() for z in corpus]

 
    stemmer = PorterStemmer()

    for z in corpus:
        stemmed_words = [stemmer.stem(word) for word in z]
        z = " ".join(stemmed_words)


    return corpus



def labelizeReviews(reviews, label_type):
    import gensim
    LabeledSentence = gensim.models.doc2vec.LabeledSentence
    labelized = []
    for i,v in enumerate(reviews):
        label = '%s_%s'%(label_type,i)
        labelized.append(LabeledSentence(v, [label]))
    
    return labelized



def getVecs(model, corpus, size):
    vecs = [np.array(model.docvecs[z.tags[0]]).reshape((1, size)) for z in corpus]
    return np.concatenate(vecs)




def main():
    
    logging.info('Loading data ...')
    train_data = []
    train_labels = []
    Dev_data = []
    Dev_labels = []
    test_data = []
    test_labels = []


    fp = open('/EnglishTrainBig/anger-train.txt', 'r')
    lines = fp.readlines()
    #headerline=0

    for line in lines:
        line=line.split('\t')
        #headerline=headerline+1
        #if(headerline==1):
            #continue

        if(len(line)==4):
            sentiment = line[3]  
            processedTweet = line[1]
          
            train_data.append(processedTweet)
            train_labels.append(sentiment)
    fp.close()

    



    fp = open('/EnglishOCTest/anger-test.txt', 'r')
    lines = fp.readlines()
    headerline=0

    for line in lines:
        line=line.split('\t')
        headerline=headerline+1
        #if(headerline==1):
            #continue

        if(len(line)==4):
            sentiment = line[3]  
            processedTweet = line[1]
          
            test_data.append(processedTweet)
            test_labels.append(sentiment)
    fp.close()




    x_train = train_data
    y_train = train_labels
    
    x_test = test_data
    y_test = test_labels



    logging.info('Labeling data ...')

    x_train = clean_text_hashtag(x_train)
    x_test = clean_text_hashtag(x_test)   

    x_train = labelizeReviews(x_train, 'TRAIN')
    x_test = labelizeReviews(x_test, 'TEST')
   


    
    training=x_train 
    testing=x_test

    allData=training+testing


    AffectTweetsTrain=[]
    x=-1   # to avoid reading the header file

    with open('/EnglishTrainBig/AffectTweets-anger-train.csv') as csvDataFile:
        csvReader = csv.reader(csvDataFile)
        for row in csvReader:
            if(x>-1):  # to avoid reading the header file
                
                AffectTweetsTrain.append(row[0:])    # the file is not good enough...go and read it to see why
                
            x=x+1
    AffectTweetsTrain=np.array(AffectTweetsTrain, dtype=float)
    



    AffectTweetsTest=[]
    x=-1
    with open('/EnglishOCTest/AffectTweets-anger-test.csv') as csvDataFile:
        csvReader = csv.reader(csvDataFile)
        for row in csvReader:
            if(x>-1):  # to avoid reading the header file
                
                AffectTweetsTest.append(row[0:])    # the file is not good enough...go and read it to see why
                
            x=x+1
    AffectTweetsTest=np.array(AffectTweetsTest, dtype=float)
    




    EmbeddingTrain=[]
    x=-1    # to avoid reading the header file

    with open('/Users/malak/Desktop/SemEval2018/lastVersionData/English/EnglishTrainBig/Embedding-anger-train.csv') as csvDataFile:
        csvReader = csv.reader(csvDataFile)
        for row in csvReader:
            if(x>-1):    # to avoid reading the header file         
                EmbeddingTrain.append(row[0:]) 
                
            x=x+1
    EmbeddingTrain=np.array(EmbeddingTrain, dtype=float)



    EmbeddingTest=[]
    x=-1    # to avoid reading the header file

    with open('/Users/malak/Desktop/SemEval2018/lastVersionData/English/EnglishOCTest/Embedding-anger-test.csv') as csvDataFile:
        csvReader = csv.reader(csvDataFile)
        for row in csvReader:
            if(x>-1):    # to avoid reading the header file         
                EmbeddingTest.append(row[0:]) 
                
            x=x+1
    EmbeddingTest=np.array(EmbeddingTest, dtype=float)




    SSTrain=[]
    x=-1    # to avoid reading the header file

    with open('/Users/malak/Desktop/SemEval2018/lastVersionData/English/EnglishTrainBig/SentiStrength-anger-train.csv') as csvDataFile:
        csvReader = csv.reader(csvDataFile)
        for row in csvReader:
            if(x>-1):    # to avoid reading the header file         
                SSTrain.append(row[0:]) 
                
            x=x+1
    SSTrain=np.array(SSTrain, dtype=float)



    SSTest=[]
    x=-1    # to avoid reading the header file

    with open('/Users/malak/Desktop/SemEval2018/lastVersionData/English/EnglishOCTest/SentiStrength-anger-test.csv') as csvDataFile:
        csvReader = csv.reader(csvDataFile)
        for row in csvReader:
            if(x>-1):    # to avoid reading the header file         
                SSTest.append(row[0:]) 
                
            x=x+1
    SSTest=np.array(SSTest, dtype=float)






    import random
    random.seed(9001)
    

 

    size = 150   #this is for doc2Vec model
    n_dim = 300 # this is for Word2Vec model
    
    #instantiate our DM and DBOW models
    logging.info('instantiate our DM and DBOW models')
    model_dm = gensim.models.Doc2Vec(size=size, min_count=1, iter=100, alpha=0.025, min_alpha=0.0001)
    model_dbow = gensim.models.Doc2Vec(size=size, min_count=1, dm=0, iter=100,alpha=0.025, min_alpha=0.0001)
    

    model_dm.build_vocab(allData)
    model_dbow.build_vocab(allData)   

    
    model_dm.train(allData, total_examples=model_dm.corpus_count , epochs=100)   ######## allData not only training
    model_dbow.train(allData, total_examples=model_dm.corpus_count , epochs=100)##### instead of epochs=model_dm.iter



    train_vecs_dm = np.array(getVecs(model_dm, training, size))
    train_vecs_dbow = np.array(getVecs(model_dbow, training, size))


    train_vecs1 = np.hstack((train_vecs_dm, train_vecs_dbow))  #300      
    train_vecs2 = np.hstack((EmbeddingTrain,AffectTweetsTrain))   #143     
    train_vecs2 = np.hstack((train_vecs2,SSTrain))   #143   + 2  
    train_vecs = np.hstack((train_vecs1,train_vecs2))


    #Construct vectors for test reviews
    test_vecs_dm = np.array(getVecs(model_dm, testing, size))
    test_vecs_dbow = np.array(getVecs(model_dbow, testing, size))
    
    test_vecs1 = np.hstack((test_vecs_dm, test_vecs_dbow))
    test_vecs2 = np.hstack((EmbeddingTest, AffectTweetsTest))  #143
    test_vecs2 = np.hstack((test_vecs2, SSTest))  #143 +2
    test_vecs = np.hstack((test_vecs1, test_vecs2))  #743



    model3 = Sequential()
    model3.add(Dense(256, input_dim=size*2+100+45, init='normal', activation='relu'))
    model3.add(Dropout(0.3))
    model3.add(Dense(256, init='normal', activation='relu'))
    model3.add(Dropout(0.5))
    model3.add(Dense(80, init='normal', activation='relu'))
    model3.add(Dense(1, init='normal', activation="sigmoid"))
    
    sgd = SGD(lr=0.01, decay=1e-6, momentum=0.9, nesterov=True)  
    model3.compile(loss='mse', optimizer=sgd, metrics=['accuracy'])   #optimizer='adam' has the same results
    earlyStopping3=EarlyStopping(monitor='val_loss', patience=10, verbose=2, mode='min')
    checkpointer3 = ModelCheckpoint(filepath="weights3.hdf5", monitor='val_loss', verbose=1, save_best_only=True, mode='min')
    print(model3.summary())
    hist=model3.fit(train_vecs, y_train, nb_epoch=40, batch_size=8, validation_split=0.2, verbose=2, callbacks=[earlyStopping3,checkpointer3])
    print "********\n\n\n\***********\n\n\n"
    model3.load_weights('weights3.hdf5')
    x_test_predictions3=model3.predict(test_vecs)



    tweet_w2v = KeyedVectors.load_word2vec_format('GoogleNews-vectors-negative300.bin', binary=True ) # , encoding='utf-8', unicode_errors='ignore')

    print 'building tf-idf matrix ...'
    vectorizer = TfidfVectorizer(analyzer=lambda x: x, min_df=1)
    matrix = vectorizer.fit_transform([x.words for x in allData])   
    tfidf = dict(zip(vectorizer.get_feature_names(), vectorizer.idf_))
    print 'vocab size :', len(tfidf)



    def getWordVector(tokens, size):
        vec = np.zeros(size).reshape((1, size))
        count = 0.
        for word in tokens:
            try:
                x_word=word.decode('utf8', errors='ignore')
                vec += tweet_w2v[x_word].reshape((1, size)) * tfidf[word]
                count += 1.
            except KeyError:
                continue

        if count != 0:
            vec /= count
        return vec
 


    from sklearn.preprocessing import scale
    train_vecs_w2v = np.concatenate([getWordVector(z, n_dim) for z in tqdm(map(lambda x: x.words, labelizeReviews(clean_text_hashtag(train_data), 'TRAIN')))])
    train_vecs_w2v = scale(train_vecs_w2v)
    train_vecs2 = EmbeddingTrain
    train_vecs3= np.hstack((AffectTweetsTrain,SSTrain))        
    train_vecs_w2v= np.hstack((train_vecs_w2v,train_vecs2))
    train_vecs_w2v= np.hstack((train_vecs_w2v,train_vecs3))

    test_vecs_w2v = np.concatenate([getWordVector(z, n_dim) for z in tqdm(map(lambda x: x.words, labelizeReviews(clean_text_hashtag(test_data), 'TEST')))])
    test_vecs_w2v = scale(test_vecs_w2v)
    test_vecs2 = EmbeddingTest
    test_vecs3= np.hstack((AffectTweetsTest,SSTest))             
    test_vecs_w2v= np.hstack((test_vecs_w2v,test_vecs2))
    test_vecs_w2v= np.hstack((test_vecs_w2v,test_vecs3))




    model = Sequential()
    model.add(Dense(256, input_dim=size*2+100+45, init='normal', activation='relu'))
    model.add(Dropout(0.3))
    model.add(Dense(256, init='normal', activation='relu'))
    model.add(Dropout(0.5))
    model.add(Dense(80, init='normal', activation='relu'))
    model.add(Dense(1, init='normal', activation="sigmoid"))
    
    sgd = SGD(lr=0.01, decay=1e-6, momentum=0.9, nesterov=True)  
    model.compile(loss='mse', optimizer=sgd, metrics=['accuracy'])   #optimizer='adam' has the same results
    earlyStopping=EarlyStopping(monitor='val_loss', patience=10, verbose=2, mode='min')
    checkpointer = ModelCheckpoint(filepath="weights.hdf5", monitor='val_loss', verbose=1, save_best_only=True, mode='min')
    print(model.summary())
    hist=model.fit(train_vecs_w2v, y_train, nb_epoch=40, batch_size=8, validation_split=0.2, verbose=2, callbacks=[earlyStopping,checkpointer])
    print "********\n\n\n\***********\n\n\n"
    model.load_weights('weights.hdf5')
    x_test_predictions=model.predict(test_vecs_w2v)



    

    num_word=0
    dic_list={}
    max_length_tweet=0
    for x in clean_text(train_data):
        if(len(x) > max_length_tweet):
            max_length_tweet=len(x)
        for y in x:
            if y in dic_list.values():
                continue
            else:
                dic_list[num_word]=y
                num_word=num_word+1
    for x in clean_text(test_data):
        if(len(x) > max_length_tweet):
            max_length_tweet=len(x)
        for y in x:
            if y in dic_list.values():
                continue
            else:
                dic_list[num_word]=y
                num_word=num_word+1



    train_data = clean_text_hashtag(train_data)
    test_data = clean_text_hashtag(test_data)   

    train_seq = np.zeros((len(train_data), max_length_tweet))
    for i in range(len(train_data)):
        for y in range(len(train_data[i])):
            if (y>=max_length_tweet):
                continue
            for key, value in dic_list.iteritems():
                if value==train_data[i][y]:
                    train_seq[i][y]=key
                    #print key
                    break
    print train_seq.shape       


    test_seq = np.zeros((len(test_data), max_length_tweet))
    for i in range(len(test_data)):
        for y in range(len(test_data[i])):
            if (y>=max_length_tweet):
                continue
            for key, value in dic_list.iteritems():
                if value==test_data[i][y]:
                    test_seq[i][y]=key
                    #print key
                    break
    print test_seq.shape       


    y_train = np.array(y_train)
    y_test = np.array(y_test)

    print len(tweet_w2v.wv.vocab ) 



    n_symbols = len(dic_list) + 1 # adding 1 to account for 0th index (for masking)
    embedding_weights = np.zeros((n_symbols, 300))
    not_found=0
    emb=0
    for key, value in dic_list.iteritems():
        try:
            value1=value.decode('utf8', errors='ignore')
            embedding_weights[key, :] = tweet_w2v.wv[value1].reshape((1, 300)) #* tfidf[value] #+tweet_w2v3.wv[value1].reshape((1, 300))+tweet_w2v2.wv[value1].reshape((1, 300)) #* tfidf[value]  #(tweet_w2v.wv[value1].reshape((1, 300)) + 
            emb=emb+1
        except KeyError:
            not_found=not_found+1
            continue




    model2 = Sequential()
    model2.add(Embedding(output_dim=300, input_dim=num_word+1, weights=[embedding_weights], input_length=max_length_tweet, trainable=False))
    model2.add(LSTM(256))  
    model2.add(Dropout(0.3))
    model2.add(Dense(256, init='normal', activation='relu'))
    model2.add(Dropout(0.5))
    model2.add(Dense(80, init='normal', activation='relu'))
    model2.add(Dense(1, init='normal', activation="sigmoid"))
    
    sgd = SGD(lr=0.01, decay=1e-6, momentum=0.9, nesterov=True)  
    model2.compile(loss='mse', optimizer=sgd, metrics=['accuracy'])   #optimizer='adam' has the same results
    earlyStopping2=EarlyStopping(monitor='val_loss', patience=10, verbose=2, mode='min')
    checkpointer2 = ModelCheckpoint(filepath="weights2.hdf5", monitor='val_loss', verbose=1, save_best_only=True, mode='min')
    print(model2.summary())
    hist=model2.fit(train_seq, y_train, nb_epoch=40, batch_size=8, validation_split=0.1,  verbose=2, callbacks=[earlyStopping2,checkpointer2])
    print "********\n\n\n\***********\n\n\n"
    model2.load_weights('weights2.hdf5')
    x_test_predictions2=model2.predict(test_seq)








    logging.info('Saving results ...\n')
    fp = open('/OriginalData/2018-EI-reg-En-test/2018-EI-reg-En-anger-test.txt', 'r')
    writing2 = open('/Results/EI-oc_en_anger_pred.txt', 'w+')
    writing7 = open('/Results/EI-reg_en_anger_pred.txt', 'w+')   #Average3     1+2+3




    lines = fp.readlines()
    i=0

    for line in lines:
        line=line.split('\t')

        if(len(line)==4):
            id=line[0]
            processedTweet = line[1]
            emo=line[2]
            sentiment = line[3]
            if(i>len(test_data)):
                sentiment=0.4
                



            if(i>0 and i<=len(test_data)):       #if it is header line
                pred=x_test_predictions[i-1][0]
                pred2=x_test_predictions2[i-1][0]
                pred3=x_test_predictions3[i-1][0]

                pred5=(pred+pred3)/2.0
                pred6=(pred2+pred3)/2.0
                pred7=(pred+pred2+pred3)/3.0
                if(pred7>=anger[0]):
                    oc="3: high amount of anger can be inferred\n"
                elif (pred7>=anger[1]):
                    oc="2: moderate amount of anger can be inferred\n"
                elif (pred7>=anger[2]):
                    oc="1: low amount of anger can be inferred\n"
                else:
                    oc="0: no anger can be inferred\n"

            else:
                pred7=sentiment
                oc="Intensity Class\n"



            if(i<=len(test_data)):
                writing2.write(id)
                writing2.write('\t')
                writing2.write(processedTweet)
                writing2.write('\t')
                writing2.write(emo)
                writing2.write('\t')
                writing2.write(oc)
                



            writing7.write(id)
            writing7.write('\t')
            writing7.write(processedTweet)
            writing7.write('\t')
            writing7.write(emo)
            writing7.write('\t')
            if(i>0):
                writing7.write(np.string_(pred7)+'\r\n')
            else:
                writing7.write(sentiment)     #if it is header



            i=i+1

            
    fp.close()



if __name__ == '__main__':
    main()

